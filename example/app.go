package main

import (
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/aher.shiva/httpretry"
)

func main() {

	url := "https://jsonplaceholder.typicode.com/posts"

	//CREATE REQUEST OBJECT
	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		log.Println("[error-create-http-request]:", err)
	}
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Authorization", "token=AG_FFhF3_jEZ3S@wf1")

	//RETRY METHOD CALL
	res, err := httpretry.Call(req, 3, 10)
	if err != nil {
		log.Println("[error-getting-response]:", err)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("[error-reading-body]:", err)
	}

	if res.StatusCode == 200 {
		log.Println("[response-body]", string(body))
	}
}
