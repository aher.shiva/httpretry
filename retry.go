package httpretry

import (
	"log"
	"net/http"
	"time"
)

//call function requires http request object, int value of no of counts, and timeout in seconds
func Call(req *http.Request, attempts, reqtimeout int32) (*http.Response, error) {
	log.Println("[retry-attemp]:", attempts)

	httpclient := http.DefaultClient
	httpclient.Timeout = time.Second * time.Duration(reqtimeout)

	var res *http.Response
	var err error
	if attempts > 0 {
		attempts = attempts - 1
		res, err = httpclient.Do(req)
		if err != nil {
			log.Println("[error-calling]:", err)
			res, err = Call(req, attempts, reqtimeout)
		}
	}
	return res, err
}
